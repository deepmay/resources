import sys, machine
led = machine.Pin(25, machine.Pin.OUT, machine.Pin.PULL_DOWN)

def userInput():
  print("Enter on or off to set LED:")
  input = sys.stdin.readline()
  if input == "on\n": 
      led.on()
      print("LED switched ON")
  elif input == "off\n": 
      led.off()
      print("LED switched OFF")
  else: pass

while True:
  userInput()