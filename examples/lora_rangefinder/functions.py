import axp202
import ssd1306
import sx127x
import os, sys
import machine
from gps import GPStime, getGPS
from machine import RTC
from math import radians, cos, sin, asin, sqrt
import time

rtc = RTC()

def Convert(numbers):
    return [ -i for i in numbers]

def power():
    axp = axp202.PMU(address=axp202.AXP192_SLAVE_ADDRESS)
    axp.setLDO2Voltage(3300)   # T-Beam LORA VDD   3v3
    axp.setLDO3Voltage(3300)   # T-Beam GPS  VDD    3v3
    axp.enablePower(axp202.AXP192_LDO3)
    axp.enablePower(axp202.AXP192_LDO2)

def displayfix(long, lat, alt, time, hs = 0):
    pass

def updateOLED():
    pass

def log(rx, fix, dist):
    file = 'gps.txt'
    dir = os.listdir()
    if file not in dir:
        f = open(file, 'w')
    else:
        f= open(file, 'a')
    line1 = 'Time: ' + str(time.time()) + " Dist: " + dist
    line2 = str(rx)
    line3 = str(fix)
    
    f.write(line1 + '\n')
    f.write('remote: ' + line2 + '\n')
    f.write('local: ' + line3 + '\n')
    f.write('\n')
    f.close()

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance in kilometers between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6378100 # Radius of earth in kilometers. Use 3956 for miles. Determines return value units.
    return c * r



def setTime():
    global gpsTime
    print("Getting GPS Time...")
    getGPS(outtime = 120)
    rtc.datetime((2022, 8, 2, 0, GPStime['hours'], GPStime['minutes'], GPStime['seconds'], 0))
    print("GPS Time Obtained: " + str(rtc.datetime()))
    


def bootup():
    power()
    setTime()

