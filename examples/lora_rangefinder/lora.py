from machine import Pin, SPI
from sx127x import SX127x
import sys
from sx127x import SX127x
import time

lora_default = {
    'frequency': 418500000,
    'frequency_offset':0,
    'tx_power_level': 14,
    'signal_bandwidth': 250e3,
    'spreading_factor': 9,
    'coding_rate': 8,
    'preamble_length': 12,
    'implicitHeader': False,
    'sync_word': 0x12,
    'enable_CRC': False,
    'invert_IQ': False,
    'debug': False,
}
lora_pins = {'dio_0':26,'cs':18,'reset':23,'sck':5,'miso':19,'mosi':27,}
lora_spi = SPI(
    baudrate=10000000, polarity=0, phase=0,
    bits=8, firstbit=SPI.MSB,
    sck=Pin(lora_pins['sck'], Pin.OUT, Pin.PULL_DOWN),
    mosi=Pin(lora_pins['mosi'], Pin.OUT, Pin.PULL_UP),
    miso=Pin(lora_pins['miso'], Pin.IN, Pin.PULL_UP),
)

lora = SX127x(lora_spi, pins=lora_pins, parameters=lora_default)

def loraTX(payload):
    print('TX >>> {}'.format(payload))
    lora.println(payload)

def loraRX():
    
    if lora.receivedPacket():
        curTime = time.time()
        try:
            payload = lora.readPayload().decode()
            rssi = lora.packetRssi()
            snr = lora.packetSnr()
            print("RX: {} | RSSI: {} | SNR: {}".format(payload, rssi, snr))
            parts = payload.split()
            rx = {'time': curTime, 'team': parts[0],'name': parts[1],'long': parts[3],'lat': parts[2], 'alt': parts[4],'sats': parts[5], 'err': parts[6], 'seq': parts[7], 'rssi': rssi, 'snr': snr}
            return rx
        except Exception as e:
            print(e)