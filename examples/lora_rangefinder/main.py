from functions import bootup, haversine, rtc, log
import _thread
import random
import time
import lora
from gps import getGPS, gpsModule
from machine import UART, Pin, I2C
import math
import time
import ssd1306

# using default address 0x3C
i2c = I2C(sda=Pin(21), scl=Pin(22))
display = ssd1306.SSD1306_I2C(128, 64, i2c)

fix = {'time': 0, 'long':0.0, 'lat':0.0, 'alt':0.0, 'sats':0, 'err':0}
maxStale = 60
team = 'animals'
name = 'dog'
seq = 0
lastTX = 0
rx = {'time': 0, 'team':'', 'name':'','long':'','lat':'', 'alt':'','sats':'', 'err':'', 'seq':'', 'rssi':'', 'snr':''}

# green,fox,40.73777627962344,-74.05247938671874,170.4,12,5,269

def get_sec(time_str):
    """Get seconds from time."""
    time = rtc.datetime()
    h = time[0][3]
    m = time[0][4]
    s = time[0 ][5]
    return int(h) * 3600 + int(m) * 60 + int(s)

def gps():
    global fix
    while True:
        parts = getGPS(outtime = 16)
        if type(parts) is list and float(parts[8]) <= 10.0:
            fix['time'] = time.time()
            fix['lat'] = parts[2]
            fix['long'] = '-' + parts[4]
            fix['sats'] = parts[7]
            fix['err'] = parts[8]
            fix['alt'] = parts[9]
            #fix['stale'] = time.time()
            print('LAT: ' + str(fix['lat']) + ' LONG: ' + str(fix['long']) + ' ALT: ' + str(fix['alt']) + ' ERR: ' + str(fix['err']))
        else:
            time.sleep(1)


def main():
    global fix, rx, lastTX, maxStale, seq
    while True:
        time.sleep_ms(70)
        curTime = time.time()
        if (curTime % 2) == 0:
            display.fill_rect(121, 0, 127, 6, 1)
            display.show()
        else:
            display.fill_rect(121, 0, 127, 6, 0)
            display.show()
        if curTime - lastTX >= 10 and curTime - fix['time'] <= 30:
                seq += 1
                elements = [team, name, str(fix['lat']), str(fix['long']), str(fix['alt']), str(fix['sats']), str(fix['err']), str(seq)]
                payload = ' '.join(elements)
                lora.loraTX(payload)
                lastTX = time.time()
        if rx := lora.loraRX():
            #display.rect(0,0,127,63, 1)
            print("RX >>> " + str(rx))
            if curTime - fix['time'] <= 10:
                #lpos = (float(fix['lat']), float(fix['long']))
                #rpos = (float(rx['lat']), float(rx['long']))
                dist = haversine(float(fix['long']), float(fix['lat']), float(rx['long']), float(rx['lat']))
                print(dist)
                print("log!")
                display.fill(0)
                display.text(rx['name'] + ' seq:' + rx['seq'], 0, 0, 1)
                display.text('d:' + str(round(dist, 0)) + 'M ' + 'e:' + rx['err'], 0, 9, 1)
                #display.text('y' + rx['lat'][0:6] + 'x' + rx['long'][0:6], 2, 12, 1)
                #display.text(' err:' + rx['err'], 0, 18, 1)
                display.text('rssi:' + str(rx['rssi']) + ' snr:' + str(rx['snr']), 0, 18, 1 )
                display.fill_rect(0, 26, 127, 63, 1)
                display.text(name, 0,26,0)
                display.text('lat: ' + fix['lat'], 0, 33, 0)
                display.text('long: ' + fix['long'], 0, 42, 0)
                display.text('sats:' + fix['sats'] + ' e:' + fix['err'], 0, 51, 0)
                display.show()
                log(rx, fix, str(round(dist, 0)))


        #print('.', end='\r')
        print(str(time.time()), end = '\r')

bootup()
_thread.start_new_thread(gps, ())
_thread.start_new_thread(main, ())
#_thread.start_new_thread(logger, ())