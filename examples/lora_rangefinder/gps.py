import sys
sys.path.insert(0, 'libraries')
import time, utime
import machine
from machine import Pin, UART, SoftI2C
from ssd1306 import SSD1306_I2C
#from haversine import Unit

i2c = SoftI2C(scl=Pin(22), sda=Pin(21), freq=10000)     #initializing the I2C method for ESP32
oled = SSD1306_I2C(128, 64, i2c)

GPS_RX_PIN = 34
GPS_TX_PIN = 12

gpsModule = machine.UART(2, rx=GPS_RX_PIN, tx=GPS_TX_PIN, baudrate=9600, bits=8, parity=None, stop=1)

print(gpsModule)
buff = bytearray(255)
TIMEOUT = False
FIX_STATUS = False
#GPStime = ""
GPStime = {'hours': 0, 'minutes': 0, 'seconds': 0}
latitude = ""
longitude = ""
altitude = ""
satellites = ""
HDOP = ""

#b'$GPGGA,121235.00,3833.54539,N,08628.63027,W,1,04,3.66,174.5,M,-32.7,M,,*62\r\n'

def getGPS(gpsModule = gpsModule, outtime = 8):
    global FIX_STATUS, TIMEOUT, latitude, longitude, altitude, satellites, GPStime, HDOP
    timeout = time.time() + outtime
    while True:
        gpsModule.readline()
        buff = str(gpsModule.readline())
        parts = buff.split(',')
        if (parts[0] == "b'$GPGGA" and len(parts) == 15):
            if(parts[1] and parts[2] and parts[3] and parts[4] and parts[5] and parts[6] and parts[7]):
                print(buff)
                parts[2] = convertToDegree(parts[2])
                parts[4] = convertToDegree(parts[4])
                altitude = parts[6]
                satellites = parts[7]
                HDOP = parts[8]
                #GPStime = parts[1][0:2] + ":" + parts[1][2:4] + ":" + parts[1][4:6]
                GPStime['hours'] = int(parts[1][0:2])
                GPStime['minutes'] = int(parts[1][2:4])
                GPStime['seconds'] = int(parts[1][4:6])
                FIX_STATUS = True
                return parts
                break
        if (time.time() > timeout):
            print('gps timeout')
            break
        time.sleep_ms(500)

def gpsTime():
#   $GPZDA,hhmmss.ss,xx,xx,xxxx,xx,xx\r\n
    timeGot = False
    while timeGot == False:
        gpsModule.readline()
        buff = str(gpsModule.readline())
        parts = buff.split(',')
        if (parts[0] == "b'$GPZDA" and len(parts) == 7):
            hours = parts[1][0:1]
            minutes = parts[1][2:3]
            seconds = parts[1][4:5]
            gpsTime = []
            gpsTime['hours'].append(hours)
            gpsTime['minutes'].append(minutes)
            gpsTime['hours'].append(seconds)
            gpsTime['day'].append(parts[2])
            gpsTime['month'].append(parts[3])
            gpsTime['year'].append(parts[4])
            timeGot = True
        else:
            time.sleep_ms(500)
    return gpsTime

        
def convertToDegree(RawDegrees):
    RawAsFloat = float(RawDegrees)
    firstdigits = int(RawAsFloat/100) 
    nexttwodigits = RawAsFloat - float(firstdigits*100) 
    Converted = float(firstdigits + nexttwodigits/60.0)
    Converted = '{0:.6f}'.format(Converted) 
    return str(Converted)