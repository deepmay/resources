import sys
sys.path.insert(0, 'libraries')
from machine import Pin, I2C
import ssd1306

# using default address 0x3C
i2c = I2C(sda=Pin(21), scl=Pin(22))
display = ssd1306.SSD1306_I2C(128, 64, i2c)

a = ((0,0,0,63),
    (0,63,127,63),
    (127,63,127,0),
    (127,0,0,0),
    (5,5,5,58),
    (5,58,122,58),
    (122,58,122,5),
    (122,5,5,5),
    (10,10,10,53),
    (10,53,117,53),
    (117,53,117,10),
    (117,10,10,10),
    (15,15,15,48),
    (15,48,112,48),
    (112,48,112,15),
    (112,15,15,15),
    (20,20,20,43),
    (20,43,107,43),
    (107,43,107,20),
    (107,20,20,20),
    (25,25,25,38),
    (25,38,102,38),
    (102,38,102,25),
    (102,25,25,25))
display.fill(0)

r = range(len(a))
draw = 0
while True:
    if draw is 0: draw = 1
    elif draw is 1: draw = 0
    for i in r:
        display.line(a[i][0],a[i][1],a[i][2],a[i][3],draw)
        display.show()
        print(str(a[i][0]) + " " + str(a[i][1]) + " " + str(a[i][2]) + " " + str(a[i][3]))


