import sys
sys.path.insert(0, 'libraries')
from libraries.sx127x import SX127x
import machine
from time import sleep
import sx127x
from lora_settings import lora1

counter = 0
while True:
    payload = 'Hello ({0})'.format(counter)
    print('TX: {}'.format(payload))
    lora1.println(payload)
    counter += 1
    sleep(3)
