# Setting up Your Environment

## Prepare your computer to connect
You're going to connect to your microcontroller using USB, but your computer needs to know how to communicate with it.
The steps here depend on what operating system your computer is running.
### Windows
Download and install the **CP210x Universal Windows Driver** here: https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers

### Linux
Your user needs to be given permission to access the serial port.

- Run this command in the terminal:
`sudo usermod -a -G dialout $USER`
or on Arch Linux:
`sudo usermod -a -G uucp $USER`

- Restart your computer.

- Plug in your microcontroller

- Run `ls /dev/tty*` in the terminal

- You should see `cu.usbserial` in the list

### macOS

- Plug in your microcontroller

- Run `ls /dev/tty*` in the terminal

- You should see either `ttyUSB0` or `ttyACM0` in the list

### Docs
If you need more detailed documentation on this process: https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/establish-serial-connection.html


## Install Your Tools

Install the Mu Editor. This is the app you'll use to write Python code and interact with the microcontroller.
- Select "ESP MicroPython" as the **Mode**

https://codewith.mu/en/download

## Flash MicroPython
Our microcontroller starts out with no firmware.

The firmware we want to run is a MicroPython interpreter. It will interpret our Python commands into microcode instructions for the hardware.

### Download the firmware file
Download the most recent release under **Firmware** here:
https://micropython.org/download/LILYGO_TTGO_LORA32/

### Flash Using Mu

- Open Mu Editor
- Plug in your microcontroller
- Select "ESP MicroPython" as the **Mode**
- Click the gear icon in the lower right to open **Mu Administration**
- Click **ESP Firmware Flasher**
- Choose **ESP32** for the **device type**
- Browse for the firmware file you downloaded
- Click **Erase & write firmware**

## Test MicroPython

- Open the Mu Editor
- Make sure your microcontroller is plugged in
- Press the reset button
- Click the **REPL** button in the top bar to open the interactive MicroPython prompt on your microcontroller
- At the `>>>` prompt, type `print("MicroPython is working!")` and press enter
- The words "MicroPython is working!" should appear on a separate line.

